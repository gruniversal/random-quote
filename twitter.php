<?php

/**
 *
 * Twitter Bot: post automatic quote on twitter
 *
 * @see: https://gitlab.com/gruniversal/random-quote/#twitter-bot
 */

// my own RandomQuote Engine (content source: https://quotefancy.com/)
require_once 'src/RandomQuoteFancy.php';
require_once 'src/RandomQuoteOutput.php';

// twitter API from https://twitteroauth.com/ (via: composer require abraham/twitteroauth)
require_once 'vendor/autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;

// config
const WORK_DIR = '/tmp';

// twitter secrets
require_once 'conf/twitter_secrets.php';

// print "disabled";
// exit;

// get current date
$today = date( 'Ymd' );
$today_image = WORK_DIR . '/randomquote_twitter_' . $today . '.jpg';

// check if today already twittered -> abort (prevent duplicate tweets)
if ( is_file( $today_image ) ) {
    http_response_code( 503 ); // Service Unavailable
    print "error: today's image is already present (assuming already tweeted)";
    exit;
}

// connect to twitter
$api = new TwitterOAuth( APP_KEY, APP_SECRET, ACCESS_TOKEN, ACCESS_SECRET );
$api->get( 'account/verify_credentials' );
if ( 200 !== $api->getLastHttpCode() ) {
    http_response_code( 401 ); // Unauthorized
    print "error: could not verify credentials";
    exit;
}

// get quote for current date
$provider = new RandomQuoteFancy();
$quote = $provider->getRandomQuoteByDay( $today );
$output = new RandomQuoteOutput( $quote );
$quote_text = $output->render( 'asString' );
$img_url = $output->render( 'asImageURL' );

// fixes apostrophes (like in https://twitter.com/r4nd0m_quot3/status/1547068425435881472)
$quote_text = str_replace('&#039;', "'", $quote_text);

// download image
$written_bytes = intval( file_put_contents( $today_image, fopen( $img_url, 'r' ) ) );
if ( 0 === $written_bytes ) {
    http_response_code( 500 ); // Internal Server Error
    print "error: could not download image file";
    exit;
}

// upload image to twitter
$upload = $api->upload( 'media/upload', [ 'media' => $today_image, 'media_type' => 'image/jpeg' ], true );
if ( 201 !== $api->getLastHttpCode() ) {
    http_response_code( 500 ); // Internal Server Error
    print "error: could not upload image file to twitter";
    exit;
}

// send tweet
$api->post( 'statuses/update', [ 'status' => $quote_text, 'media_ids' => $upload->media_id ] );
if ( 200 !== $api->getLastHttpCode() ) {
    http_response_code( 500 ); // Internal Server Error
    print "error: could not send tweet to twitter";
    //$result = $api->getLastbody();
    exit;
}

http_response_code( 200 ); // OK

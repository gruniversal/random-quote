# Random Quote

This project was started with a great demand for a daily random quote to be displayed in my browser whenever I open it.

It relies on the great quote site [Quote Fancy](https://quotefancy.com/) which is read and one quote is picked randomly.

Author:
- David Gruner, https://www.gruniversal.de

License:
- Creative Commons BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/

The quotes used for this project are provided by Quote Fancy and used in accordance with their [fair use terms](https://quotefancy.com/about/terms).

## Requirements ##

PHP > 7.0

## Quote Selection ##

You can influence the selected random quote by URL parameters:

Display the random quote for a specific day:
`index.php?day=YYYYMMDD`

Display a random quote from a specific category:
`index.php?category=elon-musk-quotes`

Display a random quote from a specific topic / tag:
`index.php?tag=business`

If none of these is given, it defaults to the current day.

## Render Methods ##

With the URL parameter `output` you can also select how the quote is displayed:

Display as website (default):
`index.php?output=website`

Display as text:
`index.php?output=string`

Display as image element:
`index.php?output=image`

Display as image url:
`index.php?output=imageurl`

Redirect to image url:
`index.php?output=redirect`

Quote selection and render methods can be mixed of course.

## Twitter Bot ##

I added a little twitter bot to this project, that relies on [twitteroauth](https://github.com/abraham/twitteroauth) and therefore integrates [composer](https://getcomposer.org/).

Run `composer update` to install the necessary vendor libraries in `vendor/`.

You also need to configure your twitter credentials in `conf/twitter_secrets.php`.

Since we don't need a lot of twitter bots doing the same, it may be much easier to follow mine: https://twitter.com/r4nd0m_quot3

## Changelog ##

### Release v0.2.1 (05.08.2022)

* fixed display of apostrophes in twitter bot

### Release v0.2.0 (06.03.2022)

* added twitter bot for daily random quotes in my timeline

### Release v0.1.0 (01.03.2022)

* changed url parameters a bit
* more intelligent caching mechanism to reduce requests
* general refactoring (separation of concerns)
* improved code readability
* added some documentation

### Release v0.0.2 (23.09.2019)

* first working version


<?php

class RandomQuoteFancy {

    private const BASE_URL = 'https://quotefancy.com/';

    // list of categories and list of quotes
    private $categories = array();
    private $quote_list = array();

    // selected category and quote
    private $category   = false;
    private $quote      = false;

    // cache directory
    private const CACHE_DIR = '/tmp';

    /*****************************************************\
     ** Public Methods
    \*****************************************************/

    /**
     * Get the daily random quote for a given day
     *
     * @param      string  $day    (optional) given day 'YYYYmmdd', default: today
     *
     * @return     array   $quote
     */
    public function getRandomQuoteByDay( string $day = '' ) {

        // default: current day
        if ( '' === $day ) {
            $day = date( 'Ymd' );
        }

        // get category list
        $this->getCategories();

        // choose a random category
        $this->selectRandomCategory( $day );

        // get quotes list for a given category
        $this->getQuotesByCategory( $this->category );

        // choose a random quote
        $this->selectRandomQuote();

        return $this->quote;
    }

    /**
     * Get a random quote by category name
     *
     * @param      string  $category_name
     *
     * @return     bool    true, false on error
     */
    public function getRandomQuoteByCategoryName( string $category_name ) {

        // get category list
        $this->getCategories();

        // get category from category name
        $this->selectCategoryByName( $category_name );

        // get quotes list for a given category
        $this->getQuotesByCategory( $this->category );

        // choose a random quote
        $this->selectRandomQuote();

        return $this->quote;
    }

    /**
     * Get a random quote for a given tag
     *
     * @param      string  $tag_name
     *
     * @return     bool    true, false on error
     */
    public function getRandomQuoteByTagName( string $tag_name ) {

        // get category list
        $this->getCategories();

        // find matching category
        $this->selectCategoryByTag( $tag_name );

        // get quotes list for a given category
        $this->getQuotesByCategory( $this->category );

        // choose a random quote
        $this->selectRandomQuote();

        return $this->quote;
    }

    /*****************************************************\
     ** Methods for retrieving data from quotefancy
    \*****************************************************/

    /**
     * Get category list from quotefancy website
     *
     * @return     int   count of categories
     */
    private function getCategories() {

        // don't do anything if categories are already there
        if ( count( $this->categories ) > 0 ) {
            return count( $this->categories );
        }

        // cache file to reduce requests and speed up quote generation
        $cache_file = self::CACHE_DIR . '/randomquote_categories.html';
        $cache_time = 30 * 86400; // days

        $cache_valid = is_readable( $cache_file ) && ( time() - filemtime( $cache_file ) < $cache_time );

        if ( false === $cache_valid ) {
            $html = file_get_contents( self::BASE_URL );
            file_put_contents( $cache_file, $html );
        }

        // reset category list
        $this->categories = array();

        // read local file and get category list (html)
        $html = file_get_contents( $cache_file );
        $matches = preg_split( '/<div class="gridblock">/i', $html );

        // reject match 0 (header)
        array_shift( $matches );

        foreach ( $matches as $key => $value ) {

            $category = [
                'url'   => '',
                'name'  => '',
                'title' => '',
                'tags'  => array()
            ];

            $tmp_chunks = preg_split( '/<div class="gridblock-/i', $value );

            // retrieve URL
            preg_match( '/href="(.*)"><img/', $tmp_chunks[0], $aux );
            $category['url'] = $aux[1];

            // retrieve name
            $category['name'] = substr( $aux[1], strlen( self::BASE_URL ) );

            // retrieve title
            preg_match( '/alt="(.*)">/', $tmp_chunks[0], $aux );
            $category['title'] = $aux[1];

            // retrieve tags
            preg_match_all( '/<a href="\/topic\/(.*)">/i', $tmp_chunks[2], $aux );
            array_shift( $aux );
            $category['tags'] = $aux[0];

            // quality check
            if ( '' !== $category['url'] ) {
                $this->categories[] = $category;
            }
        }

        return count( $this->categories );
    }

   /**
     * Get quotes for a given category from quotefancy website
     *
     * @param      array  $category
     *
     * @return     int    count of quotes
     */
    private function getQuotesByCategory( array $category ) {

        // cache file to reduce requests and speed up quote generation
        $cache_file = self::CACHE_DIR . '/randomquote_quotes_' . $category['name'] . '.html';
        $cache_time = 30 * 86400; // days

        $cache_valid = is_readable( $cache_file ) && ( time() - filemtime( $cache_file ) < $cache_time );

        if ( false === $cache_valid ) {
            $html = file_get_contents( $category['url'] );
            file_put_contents( $cache_file, $html );
        }

        // reset quote list
        $this->quote_list = array();

        // read local file and get category list (html)
        $html = file_get_contents( $cache_file );
        $matches = preg_split( '/<div class="wallpaper scrollable"/i', $html );

        // reject match 0 (header)
        array_shift( $matches );

        foreach ( $matches as $key => $value ) {

            $quote = [
                'url'   => '',
                'name'  => '',
                'title' => '',
                'img'   => ''
            ];

            $tmp_chunks = preg_split( '/"/i', $value );

            // set offet when lazy-loading is detected
            $offset = ( 'load-lazily' === $tmp_chunks[7] ) ? 2 : 0;

            // retrieve URL
            $quote['url'] = $tmp_chunks[5];

            // retrieve name
            $quote['name'] = $tmp_chunks[1];

            // retrieve title
            $quote['title'] = $tmp_chunks[9 + $offset];

            // retrieve img
            $quote['img'] = $tmp_chunks[7 + $offset];

            // quality check
            if ( 'https://' === substr( $quote['url'], 0, 8 ) ) {
                $this->quote_list[] = $quote;
            }
        }

        return count( $this->quote_list );
    }

    /*****************************************************\
     ** Methods for category selection
    \*****************************************************/

    /**
     * Get random category
     *
     * @param      integer    $seed
     *
     * @throws     Exception  if category does not exist
     *
     * @return     bool       true, if category was chosen
     */
    private function selectRandomCategory( int $seed ) {

        srand( $seed );
        $id = rand( 0, count( $this->categories ) - 1 );
        $category = $this->categories[$id];

        if ( false === is_array( $category ) ) {
            throw new Exception( "category[$id] was not found" );
            return false;
        }

        $this->category = $category;
        return true;
    }

    /**
     * Get category by category id
     *
     * @param      integer    $id     category id
     *
     * @throws     Exception  if category does not exist
     *
     * @return     bool       true, if category was chosen
     */
    private function selectCategoryById( int $id ) {

        $category = $this->categories[$id];

        if ( false === is_array( $category ) ) {
            throw new Exception( "category[$id] was not found" );
            return false;
        }

        $this->category = $category;
        return true;
    }

    /**
     * Get category by category name
     *
     * @param      string     $category_name
     *
     * @throws     Exception  if category does not exist
     *
     * @return     bool       true, if category was chosen
     */
    private function selectCategoryByName( string $category_name ) {

        foreach ( $this->categories as $id => $category ) {
            if ( $category['name'] == $category_name ) {
                $this->category = $category;
                return true;
            }
        }

        throw new Exception( "category '$category_name' was not found" );
        return false;
    }

    /**
     * Get category by tag
     *
     * @param      string     $tag_name
     *
     * @throws     Exception  if category does not exist
     *
     * @return     bool       true, if category was chosen
     */
    private function selectCategoryByTag( string $tag_name ) {

        $matched_categories = array();

        foreach ( $this->categories as $id => $category ) {
            if ( in_array( $tag_name, $category['tags'] ) ) {
                $matched_categories[] = $category;
            }
        }

        if ( count( $matched_categories ) > 0 ) {
            $id = rand( 0, count( $matched_categories ) - 1 );
            $this->category = $matched_categories[$id];
            return true;
        }

        throw new Exception( "category for tag '$tag_name' was not found" );
        return false;
    }

    /*****************************************************\
     ** Methods for quote selection
    \*****************************************************/

    /**
     * Select a random quote from retrieved quote list
     *
     * @throws     Exception  if quote list is empty
     *
     * @return     bool       true, if quote was chosen
     */
    private function selectRandomQuote() {

        // return false if no quote list present
        if ( 0 === count( $this->quote_list ) ) {
            throw new Exception( "quote list is empty" );
            return false;
        }

        // choose a random quote
        $id = rand( 0, count( $this->quote_list ) - 1 );
        $this->quote = $this->quote_list[$id];

        return true;
    }
}

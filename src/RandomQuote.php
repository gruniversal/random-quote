<?php

class RandomQuote {

    // selected quote
    private $quote      = false;

    // default mode
    private $exec_mode  = 'getRandomQuoteByDay';
    private $exec_data  = '';

    // default output type
    private $output_type = 'asWebsite';

    // quote provider class
    private $provider = null;

    // quote render class
    private $output = null;

    /**
     * Simple factory constructor (used for method chaining style)
     *
     * @return     RandomQuote
     */
    public static function init() {
        return ( new RandomQuote() );
    }

    /**
     * Sets the quote provider
     *
     * @return     self
     */
    public function setQuoteProvider() {

        $this->provider = new RandomQuoteFancy();

        return $this;
    }

    /*****************************************************\
     ** Methods for parameter handling
    \*****************************************************/

    /**
     * Evaluate given parameter array and set exec mode and output type
     *
     * @param      array  $params
     *
     * @return     self
     */
    private function evaluateParams( array $params ) {

        foreach ( $params as $param => $value ) {

            switch ( $param ) {

                // call: index.php?day=20220301
                case 'day':
                    $this->exec_mode = 'getRandomQuoteByDay';
                    $this->exec_data = preg_replace( '/[^0123456789]/i', '', $value );
                break;

                // call: index.php?category=elon-musk-quotes
                case 'category':
                    $this->exec_mode = 'getRandomQuoteByCategoryName';
                    $this->exec_data = preg_replace( '/[^a-z\-]/i', '', $value );
                break;

                // call: index.php?tag=business
                case 'tag':
                    $this->exec_mode = 'getRandomQuoteByTagName';
                    $this->exec_data = preg_replace( '/[^a-z\-]/i', '', $value );
                break;

                // call: index.php?output=
                case 'output':

                    switch ( $value ) {

                        // simple string quote
                        case 'string':
                            $this->output_type = 'asString';
                        break;

                        // url to image file
                        case 'imageurl':
                            $this->output_type = 'asImageURL';
                        break;

                        // image tag (html)
                        case 'image':
                            $this->output_type = 'asImageTag';
                        break;

                        // website (html)
                        case 'website':
                            $this->output_type = 'asWebsite';
                        break;

                        // redirect (php)
                        case 'redirect':
                            $this->output_type = 'asRedirect';
                        break;

                        // ignore any other output options
                        default:

                    }

                break;

                // ignore any other params
                default:

            }
        }

        return $this;
    }

    /**
     * Gets the random quote
     *
     * @throws     Exception   if get method does not exist
     *
     * @return     self
     */
    public function getRandomQuote() {

        // run method $mode to get quote
        if ( true === method_exists( $this->provider, $this->exec_mode ) ) {
            $this->quote = call_user_func_array( array( $this->provider, $this->exec_mode ), array( $this->exec_data ) );
        } else {
            throw new Exception( "method '" . $this->exec_mode . "' does not exist in provider '" . get_class( $this->provider ) . "'" );
        }

        return $this;
    }

    /**
     * Render quote and display output
     *
     * @return     self
     */
    public function renderOutput() {

        $this->output = new RandomQuoteOutput( $this->quote );

        $result = $this->output->render( $this->output_type );

        print $result;

        return $this;
    }

    /**
     * Run the whole thing: evaluate GET-params, get quote, generate and print output
     */
    public static function run() {

        RandomQuote::init()
            ->setQuoteProvider()
            ->evaluateParams( $_GET )
            ->getRandomQuote()
            ->renderOutput();
    }
}

<?php

class RandomQuoteOutput {

    // selected quote
    private $quote      = false;

    /**
     * Constructor
     *
     * @param      array  $quote
     */
    public function __construct( array $quote ) {
        $this->quote = $quote;
    }

    /**
     * Renders the quote with the given method
     *
     * @param      string     $method
     *
     * @throws     Exception  if $method does not exist
     *
     * @return     string     rendered output
     */
    public function render( $method ) {

        if ( true === is_callable( [ $this, $method ] ) ) {
            return $this->$method();
        } else {
            throw new Exception( "output method '" . $method . "' does not exist" );
        }

        return '';
    }

    /*****************************************************\
     ** Output methods
    \*****************************************************/

    /**
     * Represents the quote as a simple string
     *
     * @return     string  quote
     */
    private function asString() {

        if ( false === $this->quote ) {
            return '';
        }

        return $this->quote['title'];
    }

    /**
     * Represents the quote as an image url
     *
     * @return     string  quote
     */
    private function asImageURL() {

        if ( false === $this->quote ) {
            return '';
        }

        return $this->quote['img'];
    }

    /**
     * Represents the quote as a html image tag
     *
     * @return     string  quote
     */
    private function asImageTag() {

        if ( false === $this->quote ) {
            return '';
        }

        return '<img width="100%" src="' . $this->asImageURL() . '" alt="' . htmlentities( $this->asString() ) . '">';
    }

    /**
     * Represents the quote as a html image tag
     *
     * @return     string  quote
     */
    private function asWebsite() {

        if ( false === $this->quote ) {
            return '';
        }

        $html = '<!DOCTYPE html>' .
                '<html lang="de">' .
                '<head>' .
                '<meta charset="utf-8" />' .
                '<meta name="viewport" content="width=device-width, initial-scale=1.0" />' .
                '<title>Randomquote - ' . $this->asString() . '</title>' .
                '</head>' .
                '<body style="background-color: #111111">' .
                '<div style="height:95vh; width:100vw; display: flex; justify-content: center; align-items: center">' .
                '<img style="border:10px solid #ffffff; max-height: 90vh" width="80%" src="' . $this->asImageURL() . '" alt="' . htmlentities( $this->asString() ) . '">' .
                '</div>' .
                '</body>' .
                '</html>';

        return $html;
    }

    /**
     * Represents the quote as a php redirect header
     *
     * @return     string  quote
     */
    private function asRedirect() {

        if ( false === $this->quote ) {
            return false;
        }

        header( "HTTP/1.1 301 Moved Permanently" );
        header( "Location:" . $this->asImageURL() );

        return true;
    }

}

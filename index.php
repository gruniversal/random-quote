<?php

/**
 *
 * Retrieve and display a random quote from quotefancy
 *
 * @see: https://gitlab.com/gruniversal/random-quote/
 */

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

set_exception_handler(
    function ( $e ) {
        print "<pre>Exception: " . $e->getMessage() . "</pre>";
    }
);

require_once __DIR__ . '/src/RandomQuote.php';
require_once __DIR__ . '/src/RandomQuoteFancy.php';
require_once __DIR__ . '/src/RandomQuoteOutput.php';

// just run it
RandomQuote::run();
